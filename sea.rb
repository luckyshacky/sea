code = ""
loop do
  x = gets
  break if x == nil
  code += x
end
code.chomp!
out = ""
for i in code.split ","
  z = i.gsub("\t","").split ": "
  if z[0] == "if"
    out += "a(\"#{z[1].chomp}\");"
  elsif i.chomp == "|"
    out += "e();"
  elsif z[0] == "on"
    out += "o(\"#{z[1].chomp}\");"
  elsif z[0] == nil or z[1] == nil
  else
    out += "s(\"#{z[0].chomp}\",\"#{z[1].chomp}\");"
  end
end
puts out
